import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { createUser } from "../services/auth.createUser";
import { findUserByEmail } from "../services/auth.findUserByEMail";

async function registerUser(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const { email, password, name } = req.body;

    if (!email || !password || !name) {
      throw new CustomError(
        "Campos inválidos",
        400,
        "Os campos enviados são inválidos"
      );
    }

    const checkEmail = await findUserByEmail(email);
    if (checkEmail) {
      throw new CustomError("Erro no cadastro", 400, "E-mail já cadastrado");
    }

    await createUser(email, password, name);

    return res.sendStatus(200);
  } catch (error) {
    next(error);
  }
}

export { registerUser };
