import { prismaClient } from "../../../database/prismaClient";

interface Props {
  user_id: string;
  email: string;
  name: string;
}

function updateUserProfile({ email, name, user_id }: Props) {
  return prismaClient.user.update({
    data: {
      name,
      email,
    },
    where: {
      id: user_id,
    },
  });
}

export { updateUserProfile };
