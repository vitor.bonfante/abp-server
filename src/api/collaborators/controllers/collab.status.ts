import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { findCollabById } from "../services/collab.findCollabById";
import { updateCollabStatus } from "../services/collab.updateStatus";

async function CollabStatus(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const { collab_id } = req.params;

    const existCollab = await findCollabById(collab_id);
    if (!existCollab) {
      throw new CustomError(
        "Colaborador inválido",
        404,
        "Colaborador não cadastrado no sistema"
      );
    }

    await updateCollabStatus(collab_id, existCollab.is_active);

    return res.sendStatus(200);
  } catch (error) {
    next(error);
  }
}

export { CollabStatus };
