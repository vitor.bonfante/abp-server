import { prismaClient } from "../../../database/prismaClient";

function findCollabByEmail(email: string) {
  return prismaClient.collaborators.findFirst({
    where: {
      email,
    },
  });
}

export { findCollabByEmail };
