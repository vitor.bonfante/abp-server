import { Request, Response, NextFunction } from "express";
import { CustomError } from "../models/customError";

function errorHandler(
  err: TypeError | CustomError,
  req: Request,
  res: Response,
  next: NextFunction
) {
  let customError = err;
  if (!(err instanceof CustomError)) {
    customError = new CustomError(
      "Ops, estamos tendo problemas. Aguarde e tente novamente."
    );
  }

  res.status((customError as CustomError).status).send(customError);
}

export { errorHandler };
