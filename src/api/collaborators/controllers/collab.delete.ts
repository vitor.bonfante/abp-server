import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { deleteCollaborator } from "../services/collab.deleteCollab";
import { findCollabById } from "../services/collab.findCollabById";

async function deleteCollab(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  const { collab_id } = req.params;

  try {
    const collaboratorExists = await findCollabById(collab_id);

    if (!collaboratorExists) {
      throw new CustomError(
        "Não é possível deletar",
        404,
        "O colaborador informado não existe"
      );
    }

    await deleteCollaborator(collab_id);

    return res.sendStatus(200);
  } catch (error) {
    next(error);
  }
}

export { deleteCollab };
