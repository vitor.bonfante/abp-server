import { prismaClient } from "../../../database/prismaClient";

async function updateCollaborator(
  collab_id: string,
  name: string,
  email: string
) {
  const updated = await prismaClient.collaborators.update({
    where: {
      id: collab_id,
    },
    data: {
      name,
      email,
    },
  });
}
export { updateCollaborator };
