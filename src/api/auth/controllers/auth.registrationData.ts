import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { findUserById } from "../services/auth.findUserById";

async function registrationData(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const gotUserInfo = await findUserById(req.user_id as string);

    if (!gotUserInfo) {
      throw new CustomError(
        "Erro ao buscar os dados",
        400,
        "Por favor tente novamente"
      );
    }

    return res.status(200).json(gotUserInfo);
  } catch (error) {
    next(error);
  }
}

export { registrationData };
