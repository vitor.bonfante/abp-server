import { prismaClient } from "../../../database/prismaClient";

async function updateCollabStatus(collab_id: string, is_active: boolean) {
  await prismaClient.collaborators.update({
    where: {
      id: collab_id,
    },
    data: {
      is_active: !is_active,
    },
  });
}

export { updateCollabStatus };
