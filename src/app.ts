import express from "express";
import helmet from "helmet";
import cors from "cors";

import { corsOptions } from "./utils/corsOptions";
import { api } from "./api";
import { notFound } from "./middlewares/notFound";
import { errorHandler } from "./middlewares/errorHandler";

const app = express();

app.use(cors(corsOptions));
app.use(express.json());
app.use(helmet());

app.use("/api", api);

app.use(notFound);
app.use(errorHandler);

export { app };
