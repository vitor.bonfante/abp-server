import "dotenv/config";
import * as nodemailer from "nodemailer";

const transporter = nodemailer.createTransport({
  host: "smtp.mail.us-west-2.awsapps.com",
  port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL_USERNAME,
    pass: process.env.EMAIL_PASSWORD,
  },
  tls: { rejectUnauthorized: false },
});

export { transporter };
