import { prismaClient } from "../../../database/prismaClient";

async function findCollabById(collab_id: string) {
  const collab = await prismaClient.collaborators.findUnique({
    where: {
      id: collab_id,
    },
  });
  return collab;
}

export { findCollabById };
