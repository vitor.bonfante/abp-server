import { NextFunction, Response } from "express";
import { CustomRequest } from "../../../models/customRequest";
import { listCollaborators } from "../services/collab.listCollab";

async function listCollab(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const list = await listCollaborators();

    return res.status(200).json(list);
  } catch (error) {
    next(error);
  }
}

export { listCollab };
