import { NextFunction, Response } from "express";
import { CustomRequest } from "../../../models/customRequest";
import { roleCreate } from "../services/role.create";

async function roleCreationController(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  const { role_name } = req.body;

  try {
    const role = await roleCreate(role_name);

    return res.status(200).json(role);
  } catch (error) {
    next(error);
  }
}

export { roleCreationController };
