import { Router } from "express";
import { authRouter } from "./auth/auth.routes";
import { collabRouter } from "./collaborators/collab.routes";
import { roleRouter } from "./roles inacabado/roles.routes";

const api = Router();

api.use("/auth", authRouter);
// api.use("/role", roleRouter);
api.use("/collab", collabRouter);

export { api };
