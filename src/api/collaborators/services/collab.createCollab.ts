import { prismaClient } from "../../../database/prismaClient";

function createCollab(email: string, name: string) {
  return prismaClient.collaborators.create({
    data: {
      email,
      is_active: true,
      name,
      // role_id,
    },
  });
}

export { createCollab };
