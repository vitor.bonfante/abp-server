import { hashSync } from "bcrypt";
import { prismaClient } from "../../../database/prismaClient";

function changeUserPassword(user_id: string, password: string) {
  return prismaClient.user.update({
    data: {
      password_hash: hashSync(password, 12),
    },
    where: {
      id: user_id,
    },
  });
}

export { changeUserPassword };
