import { prismaClient } from "../../../database/prismaClient";

async function listAllRoles() {
  const list = await prismaClient.role.findMany();
  return list;
}

export { listAllRoles };
