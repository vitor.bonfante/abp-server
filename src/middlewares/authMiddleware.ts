import "dotenv/config";
import { Response, NextFunction } from "express";
import { verify } from "jsonwebtoken";
import { CustomError } from "../models/customError";
import { CustomRequest } from "../models/customRequest";

interface TokenModel {
  user_id: string;
}

function authMiddleware(req: CustomRequest, res: Response, next: NextFunction) {
  const { authorization } = req.headers;

  if (!authorization) {
    res.status(401);
    throw new CustomError("Token de autorização inválido ou expirado", 401, "");
  }

  let secret = process.env.TOKEN_SCT;
  if (secret) {
    try {
      // const token = authorization.split(" ")[1];
      const token = authorization;
      const payload = verify(token, secret) as TokenModel;
      req.user_id = payload.user_id;
    } catch (err) {
      res.status(401);
      throw new CustomError(
        "Token de autorização inválido ou expirado",
        401,
        ""
      );
    }
  }

  return next();
}

export { authMiddleware };
