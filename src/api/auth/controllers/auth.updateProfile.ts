import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { findUserByEmail } from "../services/auth.findUserByEMail";
import { updateUserProfile } from "../services/auth.updateUserProfile";

async function updateProfile(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const { email, name } = req.body;

    if (!email || !name) {
      throw new CustomError(
        "Campos inválidos",
        400,
        "Os campos enviados são inválidos"
      );
    }

    // const checkEmail = await findUserByEmail(email); //email sempre vai precisar ser editado
    // if (checkEmail) {
    //   throw new CustomError("Erro na edição", 400, "E-mail já utilizado");
    // }

    await updateUserProfile({
      email,
      name,
      user_id: req.user_id as string,
    });

    return res.sendStatus(200);
  } catch (error) {
    next(error);
  }
}

export { updateProfile };
