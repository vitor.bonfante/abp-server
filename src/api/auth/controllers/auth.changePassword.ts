import { compare } from "bcrypt";
import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { changeUserPassword } from "../services/auth.changePassword";
import { findUserById } from "../services/auth.findUserById";
import { findUserByIdWithHash } from "../services/auth.findUserByIdWIthHash";

async function changePassword(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const { password } = req.body;

    if (!password) {
      throw new CustomError(
        "Campos inválidos",
        400,
        "Os campos enviados são inválidos"
      );
    }

    const getUser = await findUserByIdWithHash(req.user_id as string);

    if (!getUser) {
      throw new CustomError(
        "Usuário não encontrado",
        400,
        "Seu usuário não foi encontrado na base de dados."
      );
    }

    const checkPassword = await compare(
      password,
      getUser?.password_hash as string
    );
    if (checkPassword) {
      throw new CustomError(
        "Erro na alteração",
        400,
        "Sua nova senha não deve ser igual a antiga"
      );
    }

    await changeUserPassword(req.user_id as string, password);

    return res.sendStatus(200);
  } catch (error) {
    next(error);
  }
}

export { changePassword };
