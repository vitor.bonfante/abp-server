import { Request } from "express";

interface CustomRequest extends Request {
  user_id?: string;
}

export { CustomRequest };
