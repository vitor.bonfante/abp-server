import { prismaClient } from "../../../database/prismaClient";

function findUserByEmail(email: string) {
  return prismaClient.user.findUnique({
    where: {
      email,
    },
  });
}

export { findUserByEmail };
