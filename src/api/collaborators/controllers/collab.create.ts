import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { findRoleById } from "../../roles inacabado/services/role.findRoleById";
import { createCollab } from "../services/collab.createCollab";
import { findCollabByEmail } from "../services/collab.findCollabByEmail";

async function createCollaborator(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const { name, email } = req.body;

    if (!name || !email) {
      res.status(400);
      throw new CustomError(
        "Campos inválidos",
        400,
        "Os campos enviados são inválidos"
      );
    }

    const checkEmail = await findCollabByEmail(email);
    if (checkEmail) {
      res.status(400);
      throw new CustomError("Erro no cadastro", 400, "E-mail já cadastrado");
    }

    // const checkRole = await findRoleById(role_id);
    // if (!checkRole) {
    //   res.status(400);
    //   throw new CustomError("Erro no cadastro", 400, "ID de cargo inválido");
    // }

    await createCollab(email, name);

    return res.sendStatus(200);
  } catch (error) {
    next(error);
  }
}

export { createCollaborator };
