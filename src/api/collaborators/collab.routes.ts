import { Router } from "express";
import { authMiddleware } from "../../middlewares/authMiddleware";
import { createCollaborator } from "./controllers/collab.create";
import { deleteCollab } from "./controllers/collab.delete";
import { listCollab } from "./controllers/collab.list";
import { CollabStatus } from "./controllers/collab.status";
import { updateCollab } from "./controllers/collab.update";

const collabRouter = Router();

collabRouter.post("/create", authMiddleware, createCollaborator);
collabRouter.put("/update/:collab_id", authMiddleware, updateCollab);
collabRouter.put("/updateStatus/:collab_id", authMiddleware, CollabStatus);
collabRouter.get("/list", authMiddleware, listCollab);
collabRouter.delete("/delete/:collab_id", authMiddleware, deleteCollab);
export { collabRouter };
