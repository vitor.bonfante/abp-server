import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { findCollabById } from "../services/collab.findCollabById";
import { updateCollaborator } from "../services/collab.updateCollab";

async function updateCollab(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  const { collab_id } = req.params;
  const { name, email } = req.body;
  try {
    const collaboratorExists = await findCollabById(collab_id);

    if (!collaboratorExists) {
      throw new CustomError(
        "Não é possível atualizar",
        404,
        "O colaborador informado não existe"
      );
    }

    const updated_collab = await updateCollaborator(collab_id, name, email);

    return res.status(200).json(updated_collab);
  } catch (error) {}
}

export { updateCollab };
