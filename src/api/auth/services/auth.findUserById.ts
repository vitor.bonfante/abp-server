import { prismaClient } from "../../../database/prismaClient";

function findUserById(id: string) {
  return prismaClient.user.findUnique({
    select: {
      email: true,
      name: true,
      created_at: true,
    },
    where: {
      id,
    },
  });
}

export { findUserById };
