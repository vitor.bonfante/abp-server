import "dotenv/config";
import { sign } from "jsonwebtoken";

function generateAccessToken(user_id: string) {
  let secret = process.env.TOKEN_SCT;
  if (secret) return sign({ user_id: user_id }, secret, { expiresIn: "8h" });
}

export { generateAccessToken };
