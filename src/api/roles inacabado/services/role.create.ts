import { prismaClient } from "../../../database/prismaClient";

async function roleCreate(role_name: string) {
  const role = await prismaClient.role.create({
    data: {
      name: role_name,
    },
  });
  return role;
}

export { roleCreate };
