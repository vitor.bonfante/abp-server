import { prismaClient } from "../../../database/prismaClient";

async function deleteCollaborator(collab_id: string) {
  await prismaClient.collaborators.delete({
    where: {
      id: collab_id,
    },
  });
}

export { deleteCollaborator };
