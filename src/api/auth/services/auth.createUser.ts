import { hashSync } from "bcrypt";
import { prismaClient } from "../../../database/prismaClient";

function createUser(email: string, password: string, name: string) {
  return prismaClient.user.create({
    data: {
      email,
      password_hash: hashSync(password, 12),
      name,
    },
  });
}

export { createUser };
