/*
  Warnings:

  - You are about to drop the column `role_id` on the `collaborators` table. All the data in the column will be lost.
  - You are about to drop the `roles` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "collaborators" DROP CONSTRAINT "collaborators_role_id_fkey";

-- AlterTable
ALTER TABLE "collaborators" DROP COLUMN "role_id";

-- DropTable
DROP TABLE "roles";
