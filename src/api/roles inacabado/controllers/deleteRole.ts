import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { findRoleById } from "../services/role.findRoleById";

async function deleteRole(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const { role_id } = req.params;

    const roleExists = findRoleById(role_id);
    if (!roleExists) {
      throw new CustomError(
        "Cargo inexistente",
        404,
        "Cargo a ser deletado não está registrado"
      );
    }
  } catch (error) {
    next(error);
  }
}

export { deleteRole };
