import { prismaClient } from "../../../database/prismaClient";

async function listCollaborators() {
  const list = await prismaClient.collaborators.findMany({
    orderBy: {
      name: "asc",
    },
  });
  return list;
}

export { listCollaborators };
