/*
  Warnings:

  - You are about to drop the column `is_valid_email` on the `users` table. All the data in the column will be lost.
  - You are about to drop the `access_tokens` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `role_id` to the `collaborators` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "collaborators" ADD COLUMN     "role_id" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "users" DROP COLUMN "is_valid_email";

-- DropTable
DROP TABLE "access_tokens";

-- AddForeignKey
ALTER TABLE "collaborators" ADD CONSTRAINT "collaborators_role_id_fkey" FOREIGN KEY ("role_id") REFERENCES "roles"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
