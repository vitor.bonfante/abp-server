import { Router } from "express";
import { roleCreationController } from "./controllers/createRole";

const roleRouter = Router();

roleRouter.post("/create", roleCreationController);

export { roleRouter };
