import { Router } from "express";
import { authMiddleware } from "../../middlewares/authMiddleware";
import { changePassword } from "./controllers/auth.changePassword";
import { login } from "./controllers/auth.login";
import { registerUser } from "./controllers/auth.register";
import { registrationData } from "./controllers/auth.registrationData";
import { updateProfile } from "./controllers/auth.updateProfile";

const authRouter = Router();

authRouter.post("/register", registerUser);
authRouter.post("/login", login);
authRouter.put("/updateinfo", authMiddleware, updateProfile);
authRouter.get("/userinfo", authMiddleware, registrationData);
authRouter.put("/changepassword", authMiddleware, changePassword);

export { authRouter };
