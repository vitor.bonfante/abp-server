import { compare } from "bcrypt";
import { NextFunction, Response } from "express";
import { CustomError } from "../../../models/customError";
import { CustomRequest } from "../../../models/customRequest";
import { generateAccessToken } from "../../../utils/generateToken";
import { findUserByEmail } from "../services/auth.findUserByEMail";

async function login(req: CustomRequest, res: Response, next: NextFunction) {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      res.status(400);
      throw new CustomError(
        "Campos inválidos",
        400,
        "Os campos enviados são inválidos"
      );
    }

    const checkEmail = await findUserByEmail(email);
    if (!checkEmail) {
      res.status(400);
      throw new CustomError(
        "Login inválido",
        400,
        "E-mail ou senha incorretos"
      );
    }

    const validPassword = await compare(password, checkEmail.password_hash);
    if (!validPassword) {
      res.status(400);
      throw new CustomError(
        "Login inválido",
        400,
        "E-mail ou senha incorretos"
      );
    }

    const token = await generateAccessToken(checkEmail.id);

    return res.status(200).json({
      access_token: token,
    });
  } catch (error) {
    next(error);
  }
}

export { login };
