import { NextFunction, Response } from "express";
import { CustomRequest } from "../../../models/customRequest";
import { listAllRoles } from "../services/role.list";

async function listRole(req: CustomRequest, res: Response, next: NextFunction) {
  try {
    const list = await listAllRoles();
    return res.status(200).json(list);
  } catch (error) {
    next(error);
  }
}

export { listRole };
