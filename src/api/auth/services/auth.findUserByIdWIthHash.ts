import { prismaClient } from "../../../database/prismaClient";

function findUserByIdWithHash(id: string) {
  return prismaClient.user.findUnique({
    where: {
      id,
    },
  });
}

export { findUserByIdWithHash };
