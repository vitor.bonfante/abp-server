import { prismaClient } from "../../../database/prismaClient";

function findRoleById(id: string) {
  return prismaClient.role.findUnique({
    where: {
      id,
    },
  });
}

export { findRoleById };
